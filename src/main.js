// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import firebase from 'firebase/app'
import VueClipboard from 'vue-clipboard2'
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'

Vue.use(VueClipboard)
Vue.use(VueMaterial)

Vue.config.productionTip = false

const config = {
  apiKey: 'AIzaSyDZPPto3lqX9CQntGCaBRJ3FQMU-u6i2Ak',
  authDomain: 'hai-domo-2d73a.firebaseapp.com',
  databaseURL: 'https://hai-domo-2d73a.firebaseio.com',
  projectId: 'hai-domo-2d73a',
  storageBucket: 'hai-domo-2d73a.appspot.com',
  messagingSenderId: '533213095200'
}
firebase.initializeApp(config)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
