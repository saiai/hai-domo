var characters = [
  'Renji Abarai',
  'Sosuke Aizen',
  'Alucard',
  'Amuro Ray',
  'Android17',
  'Armin Arlert',
  'Hao Asakura',
  'Astro Boy',
  'Shinn Asuka',
  'Char Aznable',
  'Sergio Batista',
  'Batou',
  'Beerus',
  'Brock',
  'Cell',
  'Hidetoshi Dekisugi',
  'Dio Brando',
  'Doraemon',
  'Ryoma Echizen',
  'Alphonse Elric',
  'Edward Elric',
  'Shirou Emiya',
  'Luke fon Fabre',
  'Frieza',
  'Gaara',
  'Gohan',
  'Goku',
  'Hayato Gokudera',
  'Gourry Gabriev',
  'Tadashi Hamada',
  'Captain Harlock',
  'Haseo',
  'Kyoya Hibari',
  'Himura Kenshin',
  'Saito Hiraga',
  'Hiro Takachiho',
  'Yoichi Hiruma',
  'Toshiro Hitsugaya',
  'Hikaru Ichijyo',
  'Gin Ichimaru',
  'Gendo Ikari',
  'Shinji Ikari',
  'Phoenix Ikki',
  'Uryu Ishida',
  'Itachi Uchiha',
  'Oscar Francois de Jarjayes',
  'Maximilian Jenius',
  'Daisuke Jigen',
  'Jiraiya',
  'Kaito Kuroba',
  'Kakashi Hatake',
  'Kamui Shiro',
  'Keiichi Morisato',
  'Kenshiro',
  'Ash Ketchum',
  'Kirito',
  'Kiritsugu Emiya',
  'Sena Kobayakawa',
  'Shinya Kogami',
  'Krillin',
  'Byakuya Kuchiki',
  'Jimmy Kudo',
  'Ichigo Kurosaki',
  'Suzaku Kururugi',
  'L',
  'Lambo',
  'Lelouch Lamperouge',
  'Lavi',
  'Legato Bluesummers',
  'Yuri Lowell',
  'Arsene Lupin',
  'Majin Buu',
  'Shogo Makishima',
  'Mello',
  'Millennium Earl',
  'Mitsuki',
  'Monkey D. Luffy',
  'Ataru Moroboshi',
  'Roy Mustang',
  'Myojin Yahiko',
  'Kaworu Nagisa',
  'Shikamaru Nara',
  'Naruto Uzumaki',
  'Natsu Dragneel',
  'Near',
  'Basara Nekki',
  'Orochimaru',
  'Piccolo',
  'Pluto',
  'Princess Sapphire',
  'Professor Ochanomizu',
  'Raoh',
  'Robita',
  'Rock',
  'Rock Lee',
  'Mukuro Rokudo',
  'Roronoa Zoro',
  'Roy Focker',
  'Ryuk',
  'Yasutora Sado',
  'Sagara Sanosuke',
  'Saito',
  'Saito Hajime',
  'Gintoki Sakata',
  'Seishiro Sakurazuka',
  'Genma Saotome',
  'Ryohei Sasagawa',
  'Tsuna Sawada',
  'Scar',
  'Pegasus Seiya',
  'Seta Sojiro',
  'Setsuna Seiei',
  'Hosuke Sharaku',
  'Shinomori Aoshi',
  'Dragon Shiryu',
  'Shishio Makoto',
  'Andromeda Shun',
  'Spike Spiegel',
  'Strider Hiryu',
  'Subaru Sumeragi',
  'Syaoran',
  'Soun Tendo',
  'Tenma',
  'Tien Shinhan',
  'Togusa',
  'Kaname Tosen',
  'Trunks',
  'Tsubasa Oozora',
  'Tuxedo Mask',
  'Sasuke Uchiha',
  'Boruto Uzumaki',
  'Vash the Stampede',
  'Vegeta',
  'Viewtiful Joe',
  'Allen Walker',
  'Kimihiro Watanuki',
  'Derek Wildstar',
  'Nicholas Wolfwood',
  'Xellos',
  'Light Yagami',
  'Tai Kamiya',
  'Takeshi Yamamoto',
  'Kira Yamato',
  'Yamcha',
  'Eren Yeager',
  'Yoh Asakura',
  'Youji Itami',
  'Yu Kanda',
  'Yukishiro Enishi',
  'Athrun Zala',
  'Zamasu',
  'Kenpachi Zaraki',
  'Kiyo Takamine',
  'Zatch Bell',
  'Koichi Zenigata',
  'Homura Akemi',
  'Misa Amane',
  'Android18',
  'Asuna',
  'Athena',
  'Cagalli Yula Athha',
  'Rei Ayanami',
  'Belldandy',
  'Bulma',
  'C.C.',
  'Chi',
  'Lacus Clyne',
  'Dejiko',
  'Chrome Dokuro',
  'Haruhi Fujioka',
  'Sakura Haruno',
  'Misa Hayase',
  'Hestia',
  'Hinata Hyuga',
  'Yuko Ichihara',
  'Orihime Inoue',
  'Lum Invader',
  'Lina Inverse',
  'Milia Fallyna Jenius',
  'Mylene Flare Jenius',
  'Kamiya Kaoru',
  'Madoka Kaname',
  'Misato Katsuragi',
  'Tomie Kawakami',
  'Sakura Kinomoto',
  'Miyuki Kobayakawa',
  'Yotsuba Koiwai',
  'Rukia Kuchiki',
  'Motoko Kusanagi',
  'Anna Kyoyama',
  'Lady Snowblood',
  'Nunnally Lamperouge',
  'Leafa',
  'Lenalee Lee',
  'Lucy Heartfilia',
  'Michiko',
  'Hatchin',
  'Fujiko Mine',
  'Lynn Minmay',
  'Makimachi Misao',
  'Misty',
  'Moka Akashiya',
  'Yuki Nagato',
  'Nami',
  'Naru Narusegawa',
  'Nausicaa',
  'Himari Noihara',
  'Arale Norimaki',
  'Yomiko Readman',
  'Rias Gremory',
  'Saber',
  'Sakura',
  'Caerula Sanguis',
  'Nodoka Saotome',
  'Shampoo',
  'Sinon',
  'Skuld',
  'Asuka Langley Soryu',
  'Kallen Stadtfeld',
  'Super Sonico',
  'Haruhi Suzumiya',
  'Akane Tendo',
  'Kasumi Tendo',
  'Nabiki Tendo',
  'Natsumi Tsujimoto',
  'Akane Tsunemori',
  'Sarada Uchiha',
  'Urd',
  'Winry Rockbell'
]
var character = function () {
  var unformatted = characters[Math.floor(Math.random() * characters.length)]
  var parts = unformatted.split(' ')
  var result = 'Makoto' // default to waifu, because obviously
  if (parts !== undefined) {
    result = parts[Math.floor(Math.random() * parts.length)]
  }
  return result.charAt(0).toUpperCase() + result.substring(1)
}
export default character
