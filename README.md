# Hai domo

> https://haido.moe/ is a URL shortening/forwarding app built by a weeb for weebs. It is built using Vue, deployed via Gitlab CI, hosted with Firebase, and uses Firestore for its backing storage.

## Dev

Build and deploy locally:
``` bash
git clone https://gitlab.com/saiai/hai-domo.git
cd hai-domo
yarn install
yarn start
```

Run tests:
``` bash
yarn test
yarn e2e
```

Run lint:
``` bash
yarn lint
```
